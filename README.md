 Control.Empty
 ===

 A type class for types of kind `* -> *`, with laws related to other classes.
